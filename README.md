# README #

This is a proof of concept, showing a GO based lambda that can send SOAP messages to the AWS Account Service.

Lambda function was created manually, but when it's time to automate
consider https://github.com/awsdocs/aws-lambda-developer-guide/blob/master/sample-apps/blank-go/2-deploy.sh
