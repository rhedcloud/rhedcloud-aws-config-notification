package accountnotification

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"rhedcloud.org/aws-config/notification/soap"
	"time"
)

type Error struct {
	ErrorNumber      *string `xml:"ErrorNumber,omitempty"`
	ErrorDescription *string `xml:"ErrorDescription,omitempty"`
	Type             string  `xml:"type,attr,omitempty"`
}
type Result struct {
	Error  []*Error `xml:"Error,omitempty"`
	Action string   `xml:"action,attr,omitempty"`
	Status string   `xml:"status,attr,omitempty"`
}
type EmailAddress struct {
	Type  *string `xml:"Type,omitempty"`
	Email *string `xml:"Email,omitempty"`
}
type Property struct {
	Key   *string `xml:"Key,omitempty"`
	Value *string `xml:"Value,omitempty"`
}
type Account struct {
	AccountId              *string         `xml:"AccountId,omitempty"`
	AccountName            *string         `xml:"AccountName,omitempty"`
	ComplianceClass        *string         `xml:"ComplianceClass,omitempty"`
	PasswordLocation       *string         `xml:"PasswordLocation,omitempty"`
	EmailAddress           []*EmailAddress `xml:"EmailAddress,omitempty"`
	Property               []*Property     `xml:"Property,omitempty"`
	AccountOwnerId         *string         `xml:"AccountOwnerId,omitempty"`
	FinancialAccountNumber *string         `xml:"FinancialAccountNumber,omitempty"`
	CreateUser             *string         `xml:"CreateUser,omitempty"`
	CreateDatetime         *string         `xml:"CreateDatetime,omitempty"`
	LastUpdateUser         *string         `xml:"LastUpdateUser,omitempty"`
	LastUpdateDatetime     *string         `xml:"LastUpdateDatetime,omitempty"`
}
type AccountReply struct {
	XMLName xml.Name
	Result  *Result    `xml:"Result,omitempty"`
	Account []*Account `xml:"Account,omitempty"`
}

type Parameter struct {
	Name  string `xml:"name,attr,omitempty"`
	Value string `xml:"value,attr,omitempty"`
}
type QueryLanguage struct {
	Parameter []*Parameter `xml:"Parameter,omitempty"`
	Type      string       `xml:"type,attr,omitempty"`
	Name      string       `xml:"name,attr,omitempty"`
	Value     string       `xml:"value,attr,omitempty"`
}
type Comparison struct {
	Operator string `xml:"operator,attr,omitempty"`
	Fields   string `xml:"fields,attr,omitempty"`
	Data     string `xml:"data,attr,omitempty"`
}

type AccountQuerySpecification struct {
	Comparison             []*Comparison  `xml:"Comparison,omitempty"`
	QueryLanguage          *QueryLanguage `xml:"QueryLanguage,omitempty"`
	AccountId              *string        `xml:"AccountId,omitempty"`
	AccountName            *string        `xml:"AccountName,omitempty"`
	EmailAddress           *EmailAddress  `xml:"EmailAddress,omitempty"`
	AccountOwnerId         *string        `xml:"AccountOwnerId,omitempty"`
	FinancialAccountNumber *string        `xml:"FinancialAccountNumber,omitempty"`
	CreateUser             *string        `xml:"CreateUser,omitempty"`
	LastUpdateUser         *string        `xml:"LastUpdateUser,omitempty"`
}
type AccountQueryRequest struct {
	XMLName                   xml.Name                   `xml:"http://www.rhedcloud.org/AwsAccountService/ AccountQueryRequest"`
	AccountQuerySpecification *AccountQuerySpecification `xml:"AccountQuerySpecification,omitempty"`
}

type Annotation struct {
	Text               string `xml:"Text,omitempty"`
	CreateUser         string `xml:"CreateUser,omitempty"`
	CreateDatetime     string `xml:"CreateDatetime,omitempty"`
	LastUpdateUser     string `xml:"LastUpdateUser,omitempty"`
	LastUpdateDatetime string `xml:"LastUpdateDatetime,omitempty"`
}
type AccountNotification struct {
	AccountNotificationId string       `xml:"AccountNotificationId,omitempty"`
	AccountId             string       `xml:"AccountId,omitempty"`
	Type                  string       `xml:"Type,omitempty"`
	Priority              string       `xml:"Priority,omitempty"`
	Subject               string       `xml:"Subject,omitempty"`
	Text                  string       `xml:"Text,omitempty"`
	ReferenceId           string       `xml:"ReferenceId,omitempty"`
	Annotation            []Annotation `xml:"Annotation,omitempty"`
	CreateUser            string       `xml:"CreateUser,omitempty"`
	CreateDatetime        string       `xml:"CreateDatetime,omitempty"`
	LastUpdateUser        string       `xml:"LastUpdateUser,omitempty"`
	LastUpdateDatetime    string       `xml:"LastUpdateDatetime,omitempty"`
}
type AccountNotificationRequest struct {
	XMLName             xml.Name            `xml:"http://www.rhedcloud.org/AwsAccountService/ AccountNotificationRequest"`
	AccountNotification AccountNotification `xml:"AccountNotification,omitempty"`
}

var endpoint = "/services/AwsAccountService"

func Create(serviceUrlBase string, username string, password string, notification AccountNotification) error {
	// service url
	url := serviceUrlBase + endpoint

	// datetime is formatted as 2020-06-11T13:18:20.017-06:00
	datetime := time.Now().Format("2006-01-02T15:04:05.000Z07:00")

	// soap action and payload
	soapAction := "http://www.rhedcloud.org/AwsAccountService/AccountNotificationCreate"
	soapMsgTemplate := `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aws="http://www.rhedcloud.org/AwsAccountService/" xmlns:aws1="http://www.rhedcloud.org/awsaccount/">
		   <soapenv:Header/>
		   <soapenv:Body>
			  <aws:AccountNotificationRequest>
				 <aws1:AccountNotification>
					<aws1:AccountId>%s</aws1:AccountId>
					<aws1:Type>%s</aws1:Type>
					<aws1:Priority>%s</aws1:Priority>
					<aws1:Subject>%s</aws1:Subject>
					<aws1:Text>%s</aws1:Text>
					<aws1:Annotation>
					   <aws1:Text>%s</aws1:Text>
					   <aws1:CreateUser>%s</aws1:CreateUser>
					   <aws1:CreateDatetime>%s</aws1:CreateDatetime>
					</aws1:Annotation>
					<aws1:CreateUser>%s</aws1:CreateUser>
					<aws1:CreateDatetime>%s</aws1:CreateDatetime>
				 </aws1:AccountNotification>
			  </aws:AccountNotificationRequest>
		   </soapenv:Body>
		</soapenv:Envelope>`
	// for now hard code the 843017634583 (emory-test-3) account
	soapMsg := fmt.Sprintf(soapMsgTemplate,
		/*notification.AccountId*/ "843017634583", notification.Type, notification.Priority,
		notification.Subject, notification.Text,
		notification.Annotation[0].Text, notification.CreateUser, datetime,
		notification.CreateUser, datetime)

	/*
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
		   <soapenv:Body>
		      <ns1:GenericReply xmlns:ns1="http://www.openeai.org/openeai/">
		         <ns1:Result action="Create" status="success"/>
		         <ns1:ReplyMessage><![CDATA[<Result action="Create" status="success"><ProcessedMessageId><SenderAppId>org.rhedcloud.AwsAccountService</SenderAppId><ProducerId>4771f38d-52a6-4edc-8f10-d353d67871c0</ProducerId><MessageSeq>167</MessageSeq></ProcessedMessageId></Result>]]></ns1:ReplyMessage>
		      </ns1:GenericReply>
		   </soapenv:Body>
		</soapenv:Envelope>
	*/
	/*
		<?xml version='1.0' encoding='UTF-8'?>
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
			<soapenv:Body>
				<ns1:GenericReply xmlns:ns1="http://www.openeai.org/openeai/">
					<ns1:Result action="Create" status="success" />
					<ns1:ReplyMessage>&lt;Result action="Create" status="success">&lt;ProcessedMessageId>&lt;SenderAppId>org.rhedcloud.AwsAccountService&lt;/SenderAppId>&lt;ProducerId>4771f38d-52a6-4edc-8f10-d353d67871c0&lt;/ProducerId>&lt;MessageSeq>171&lt;/MessageSeq>&lt;/ProcessedMessageId>&lt;/Result></ns1:ReplyMessage>
				</ns1:GenericReply>
			</soapenv:Body>
		</soapenv:Envelope>
	*/
	reader := bytes.NewReader([]byte(soapMsg))
	req, err := http.NewRequest(http.MethodPost, url, reader)
	if err != nil {
		log.Printf("Error on creating request object. %s\n", err.Error())
		return errors.New(fmt.Sprintf("Error on creating request object. %s", err.Error()))
	}

	// set the content type header, as well as the oter required headers
	req.Header.Set("Content-type", "text/xml")
	req.Header.Set("SOAPAction", soapAction)

	// authorization credentials
	req.SetBasicAuth(username, password)

	// prepare the client request
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	log.Println("-> Dispatching the request to " + url)

	// dispatch the request
	res, err := client.Do(req)
	if err != nil {
		log.Printf("Error on dispatching request. %s\n", err.Error())
		return errors.New(fmt.Sprintf("Error on dispatching request. %s", err.Error()))
	}

	log.Println("-> Retrieving and parsing the response")

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Printf("Error reading response body. %s\n", err.Error())
		return errors.New(fmt.Sprintf("Error reading response body. %s", err.Error()))
	}
	err = res.Body.Close()
	if err != nil {
		// ignoring
	}

	// read and parse the response body
	soapEnvelope := &soap.SoapEnvelope{}
	err = xml.Unmarshal(body, soapEnvelope)
	if err != nil {
		log.Printf("Error on Unmarshal body. %s\n", err.Error())
		return errors.New(fmt.Sprintf("Error on Unmarshal body. %s", err.Error()))
	}
	// XXX - check for Fault

	return nil
}
