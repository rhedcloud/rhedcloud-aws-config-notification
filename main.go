package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
	"math/rand"
	"rhedcloud.org/aws-config/notification/accountnotification"
	"rhedcloud.org/aws-config/notification/secret"
)

/*
 * https://docs.aws.amazon.com/lambda/latest/dg/lambda-golang.html
 */
func handleRequest(ctx context.Context, event events.SNSEvent) (string, error) {
	//eventJson, _ := json.MarshalIndent(event, "", "  ")
	//log.Printf("KHB SNS EVENT: %s", eventJson)

	//serviceUrlBase := os.Getenv("serviceUrlBase")
	serviceUrlBase := "https://rhedcloud-awsaccount-service-test.ws.serviceforge.net"

	// username/password for WS authentication
	username, password, err := secret.GetSecret("aws-config-notification", "us-east-1")
	if err != nil {
		log.Printf("Error getting secret. %v\n", err)
		return "", err
	}

	for i, r := range event.Records {
		var msgMap map[string]json.RawMessage
		// top level fields in a ComplianceChangeNotification event
		var messageType string
		var awsAccountId string
		var awsRegion string
		var configRuleName string
		var resourceType string
		var resourceId string
		// newEvaluationResult/evaluationResultIdentifier/evaluationResultQualifier
		var evaluationResult map[string]json.RawMessage
		var complianceType string

		err := json.Unmarshal([]byte(r.SNS.Message), &msgMap)
		if err != nil {
			log.Println("Error unmarshalling message")
			return "", err
		}
		err = json.Unmarshal(msgMap["messageType"], &messageType)
		if err != nil {
			log.Println("Error unmarshalling messageType")
			return "", err
		}

		log.Printf("[%d] MessageType %s\n", i, messageType)

		// https://docs.aws.amazon.com/config/latest/developerguide/example-config-rule-compliance-notification.html
		if messageType == "ComplianceChangeNotification" {
			err = json.Unmarshal(msgMap["awsAccountId"], &awsAccountId)
			if err != nil {
				log.Println("Error unmarshalling awsAccountId")
				return "", err
			}
			err = json.Unmarshal(msgMap["awsRegion"], &awsRegion)
			if err != nil {
				log.Println("Error unmarshalling awsRegion")
				return "", err
			}
			err = json.Unmarshal(msgMap["configRuleName"], &configRuleName)
			if err != nil {
				log.Println("Error unmarshalling configRuleName")
				return "", err
			}
			err = json.Unmarshal(msgMap["resourceType"], &resourceType)
			if err != nil {
				log.Println("Error unmarshalling resourceType")
				return "", err
			}
			err = json.Unmarshal(msgMap["resourceId"], &resourceId)
			if err != nil {
				log.Println("Error unmarshalling resourceId")
				return "", err
			}

			err = json.Unmarshal(msgMap["newEvaluationResult"], &evaluationResult)
			if err != nil {
				log.Println("Error unmarshalling newEvaluationResult")
				return "", err
			}
			err = json.Unmarshal(evaluationResult["complianceType"], &complianceType)
			if err != nil {
				log.Println("Error unmarshalling complianceType")
				return "", err
			}
			if complianceType == "NON_COMPLIANT" {
				// like SRDOBJECT,ServiceMonitoring,arn:aws:service:us-east-1:843017634583:resource2
				arn := fmt.Sprintf("arn:aws:%s:%s:%s:%s/%d", resourceType, awsRegion, awsAccountId, resourceId, rand.Intn(1000))
				annotationText := fmt.Sprintf("SRDOBJECT,%s,%s", configRuleName, arn)

				an := accountnotification.AccountNotification{
					AccountId:  awsAccountId,
					Type:       "SRD",
					Priority:   "High",
					Subject:    "Found a ComplianceChangeNotification",
					Text:       "Found it from GO lambda",
					Annotation: []accountnotification.Annotation{{Text: annotationText}},
					CreateUser: "rhedcloud-aws-config-notification",
				}

				log.Printf("%s ComplianceChangeNotification AccountNotification %v\n", complianceType, an)

				err = accountnotification.Create(serviceUrlBase, username, password, an)
				if err != nil {
					log.Printf("Error: %v\n", err)
					return "", err
				}
			} else {
				log.Printf("%s ComplianceChangeNotification\n", complianceType)
			}
		}
	}
	return "", nil
}

func main() {
	lambda.Start(handleRequest)
}
