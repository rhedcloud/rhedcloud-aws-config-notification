package secret

// Use this code snippet in your app.
// If you need more information about configurations or implementing the sample code, visit the AWS docs:
// https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/setting-up.html

import (
	"encoding/json"
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

func GetSecret(secretName string, region string) (username string, password string, err error) {
	//Create a Secrets Manager client
	awsSession, err := session.NewSession()
	if err != nil {
		return "", "", err
	}
	svc := secretsmanager.New(awsSession, aws.NewConfig().WithRegion(region))
	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretName),
		VersionStage: aws.String("AWSCURRENT"), // VersionStage defaults to AWSCURRENT if unspecified
	}

	// In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
	// See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html

	result, err := svc.GetSecretValue(input)
	if err != nil {
		return "", "", err
	}

	if result.SecretString == nil {
		return "", "", errors.New("unexpected binary secret")
	}
	var credentialsMap map[string]string
	err = json.Unmarshal([]byte(*result.SecretString), &credentialsMap)
	if err != nil {
		return "", "", err
	}
	return credentialsMap["username"], credentialsMap["password"], nil
}
