package soap

import "encoding/xml"

type SoapEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  *SoapHeader
	Body    *SoapBody
}
type SoapHeader struct {
	XMLName  struct{} `xml:"Header"`
	Contents []byte   `xml:",innerxml"`
}
type SoapFault struct {
	XMLName     xml.Name `xml:"Fault"`
	Faultcode   string   `xml:"faultcode"`
	Faultstring string   `xml:"faultstring"`
}
type SoapBody struct {
	XMLName      xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`
	Response     []byte   `xml:",innerxml"`
	FaultDetails *SoapFault
}
